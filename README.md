Interface traduction objets
===========

![logo](./prive/themes/spip/images/interface_traduction_objets-128.png)


Rend la traduction de vos objets encore plus simple



Documentation:\
https://contrib.spip.net/5133
