<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/interface_traduction_objets?lang_cible=fr_tu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_desactiver_interface_traduction' => 'Désactiver l’interface de traduction',
	'info_desactiver_liste_compacte' => 'Désactiver les listes compactes',
	'interface_traduction_objets_titre' => 'Interface de traduction pour objets',

	// O
	'options' => 'Modifier les options de langues',

	// T
	'titre_page_configurer_interface_traduction_objets' => 'Configuration',
	'traductions' => 'Trad',

	// V
	'voir_traductions' => 'Voir le détail des traductions'
);
